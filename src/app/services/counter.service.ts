import { Injectable } from '@angular/core';

@Injectable()
export class CounterService {
  toActive = 0;
  toInactive = 0;
  constructor() { }

  logStatusChange(status: string) {
    if (status === 'inactive') {
      this.toInactive++;
      console.log('active->inactive: ' + this.toInactive);
    } else {
      this.toActive++;
      console.log('inactive->active: ' + this.toActive);
    }
  }

}
